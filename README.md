<div align="center">
  
<h1> <a href="https://qiplex.com/software/easy-file-organizer/">Easy File Organizer</a> </h1>

<h3> One-click file organizer for Windows, macOS and Linux </h3>

You can get the app on 
<a href="https://gitlab.com/Qiplex-Apps/easy-file-organizer/-/releases">GitLab</a>
or on 
<a href="https://qiplex.com/software/easy-file-organizer/">my website</a>
<br>Code comes soon.


![Easy File Organizer](https://qiplex.com/assets/img/app/main/easy-file-organizer-app.png)

<h4> Check out the app features below: </h4>

![Easy File Organizer - Features](https://user-images.githubusercontent.com/32670415/147217649-e4a9537a-152e-4a8e-8461-37ffbe589673.png)
